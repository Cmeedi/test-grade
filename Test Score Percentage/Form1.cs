﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Test_Score_Percentage
{
    public partial class GradeForm : Form
    {
        public GradeForm()
        {
            InitializeComponent();
        }

        private void DetermineGradeButton_Click(object sender, EventArgs e)
        {

            try
            {
                decimal score;

                score = decimal.Parse(TestScoreTextBox.Text);


                if (score <= 60)
                {
                    YourGradeResult.Text = "F";
                }
                else
                {
                    if (score <= 70)
                    {
                        YourGradeResult.Text = "D";
                    }
                    else
                    {
                        if (score <= 80)
                        {
                            YourGradeResult.Text = "C";
                        }
                        else
                        {
                            if (score <= 90)
                            {
                                YourGradeResult.Text = "B";
                            }
                            else
                            {
                                if (score <= 100)
                                {
                                    YourGradeResult.Text = "A";
                                }
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void CancelButton1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
