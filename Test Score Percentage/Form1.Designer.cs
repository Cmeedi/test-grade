﻿namespace Test_Score_Percentage
{
    partial class GradeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TestScoreTextBox = new System.Windows.Forms.TextBox();
            this.EnterYourTestScoreLabel = new System.Windows.Forms.Label();
            this.YourGradeLabel = new System.Windows.Forms.Label();
            this.YourGradeResult = new System.Windows.Forms.Label();
            this.DetermineGradeButton = new System.Windows.Forms.Button();
            this.CancelButton1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // TestScoreTextBox
            // 
            this.TestScoreTextBox.Location = new System.Drawing.Point(166, 23);
            this.TestScoreTextBox.Name = "TestScoreTextBox";
            this.TestScoreTextBox.Size = new System.Drawing.Size(100, 22);
            this.TestScoreTextBox.TabIndex = 0;
            // 
            // EnterYourTestScoreLabel
            // 
            this.EnterYourTestScoreLabel.AutoSize = true;
            this.EnterYourTestScoreLabel.Location = new System.Drawing.Point(20, 26);
            this.EnterYourTestScoreLabel.Name = "EnterYourTestScoreLabel";
            this.EnterYourTestScoreLabel.Size = new System.Drawing.Size(140, 17);
            this.EnterYourTestScoreLabel.TabIndex = 1;
            this.EnterYourTestScoreLabel.Text = "Enter your test score";
            // 
            // YourGradeLabel
            // 
            this.YourGradeLabel.AutoSize = true;
            this.YourGradeLabel.Location = new System.Drawing.Point(42, 82);
            this.YourGradeLabel.Name = "YourGradeLabel";
            this.YourGradeLabel.Size = new System.Drawing.Size(79, 17);
            this.YourGradeLabel.TabIndex = 2;
            this.YourGradeLabel.Text = "Your grade";
            // 
            // YourGradeResult
            // 
            this.YourGradeResult.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.YourGradeResult.Location = new System.Drawing.Point(166, 82);
            this.YourGradeResult.Name = "YourGradeResult";
            this.YourGradeResult.Size = new System.Drawing.Size(100, 17);
            this.YourGradeResult.TabIndex = 3;
            // 
            // DetermineGradeButton
            // 
            this.DetermineGradeButton.Location = new System.Drawing.Point(12, 130);
            this.DetermineGradeButton.Name = "DetermineGradeButton";
            this.DetermineGradeButton.Size = new System.Drawing.Size(109, 49);
            this.DetermineGradeButton.TabIndex = 4;
            this.DetermineGradeButton.Text = "Determine Grade";
            this.DetermineGradeButton.UseVisualStyleBackColor = true;
            this.DetermineGradeButton.Click += new System.EventHandler(this.DetermineGradeButton_Click);
            // 
            // CancelButton1
            // 
            this.CancelButton1.Location = new System.Drawing.Point(160, 130);
            this.CancelButton1.Name = "CancelButton1";
            this.CancelButton1.Size = new System.Drawing.Size(109, 49);
            this.CancelButton1.TabIndex = 5;
            this.CancelButton1.Text = "Cancel";
            this.CancelButton1.UseVisualStyleBackColor = true;
            this.CancelButton1.Click += new System.EventHandler(this.CancelButton1_Click);
            // 
            // GradeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(281, 201);
            this.Controls.Add(this.CancelButton1);
            this.Controls.Add(this.DetermineGradeButton);
            this.Controls.Add(this.YourGradeResult);
            this.Controls.Add(this.YourGradeLabel);
            this.Controls.Add(this.EnterYourTestScoreLabel);
            this.Controls.Add(this.TestScoreTextBox);
            this.Name = "GradeForm";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TestScoreTextBox;
        private System.Windows.Forms.Label EnterYourTestScoreLabel;
        private System.Windows.Forms.Label YourGradeLabel;
        private System.Windows.Forms.Label YourGradeResult;
        private System.Windows.Forms.Button DetermineGradeButton;
        private System.Windows.Forms.Button CancelButton1;
    }
}

